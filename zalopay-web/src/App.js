import React, { Component } from 'react';
import { 
  Row, 
  Col,
  Button,
  ButtonGroup,
  Input,
  InputGroupAddon,
  Dropdown, 
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
  Form,
  FormGroup,
  Label
} from 'reactstrap';

import {
  BrowserRouter as Router,
  Route,
}
from 'react-router-dom';

import './App.css';

const asset = {
  'icon_napdt': require('./Assets/icon_napdt.png'),
  'icon_chiatiennhom': require('./Assets/icon_chiatiennhom.png'),
  'icon_chuyentien': require('./Assets/icon_chuyentien.png'),
  'icon_dichvu': require('./Assets/icon_dichvu.png'),
  'icon_dien': require('./Assets/icon_dien.png'),
  'icon_guiquamung': require('./Assets/icon_guiquamung.png'),
  'icon_internet': require('./Assets/icon_internet.png'),
  'icon_muasam': require('./Assets/icon_muasam.png'),
  'icon_nhantien': require('./Assets/icon_nhantien.png'),
  'icon_nuoc': require('./Assets/icon_nuoc.png'),
  'icon_thedichvu': require('./Assets/icon_thedichvu.png'),
  'icon_thedt': require('./Assets/icon_thedt.png'),
  'icon_tranothe': require('./Assets/icon_tranothe.png'),
  'icon_truyenhinh': require('./Assets/icon_truyenhinh.png'),
  'icon_vemaybay': require('./Assets/icon_vemaybay.png'),
  'icon_vephim': require('./Assets/icon_vephim.png'),
}

// button image text
class ButtonImageText extends React.Component {
  render () {
    return (
      <div className="BtnImg">
        <a href={this.props.link}>
          <img src={asset[this.props.imgName]} alt='button'/>
        </a>
        <div>{this.props.name}</div>      
      </div>
    );
  }
}

// header
class Header extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }));
  }
  render () {
    return (              
      <div className="Header">
        <Row className="rowHeader">
          <Col xs='2'>
            <a href="/" component={Home}>
              <img src={require('./Assets/logo_zalopay.png')} alt='logo' style={{widh: 115, height: 30}}/>
            </a>
          </Col>
          <Col xs='4'>
            <InputGroupAddon addonType="append">
              <Input type="text" name="search" id="search" placeholder="Gõ để tìm kiếm ..." />
              <Button>Tìm kiếm</Button>
            </InputGroupAddon>
          </Col>
          <Col xs='4'>
            <ButtonGroup>
              <Button><a className="Link" href="/thanhtoan">Thanh toán</a></Button>
              <Button><a className="Link" href="/sodu">Số dư</a></Button>
              <Button><a className="Link" href="/nganhang">Ngân hàng</a></Button>
            </ButtonGroup>
          </Col>
          <Col xs='2'>
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
              <DropdownToggle caret>
                Cá nhân
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem><a className="LinkItem" href="/tkzalopay">TK ZaloPay</a></DropdownItem>
                <DropdownItem><a className="LinkItem" href="/nganhang">Ngân Hàng</a></DropdownItem>
                <DropdownItem><a className="LinkItem" href="/guiquamung">Quà tặng</a></DropdownItem>
                <DropdownItem><a className="LinkItem" href="/caidat">Cài đặt</a></DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </Col>
        </Row>
      </div>
    );
  }
}

// footer
class Footer extends React.Component {
  render () {
    return (
      <div className="Footer">
        <Row>
          <Col xs='8'>
            <div className="textInfo">
              <ul>
                <li>Công ty Cổ phần ZION</li>
                <li>Trụ sở: 52 Nguyễn Ngọc Lộc, P.14, Q.10, Tp.HCM, VN.</li>
                <li>Giấy phép Cung ứng dịch vụ trung gian thanh toán số 19/GP-NHNN do Ngân Hàng Nhà Nước cấp ngày 18/01/2016</li>
              </ul>   
            </div>
          </Col>
          <Col xs='4'>
            <a href='https://facebook.com/zalopay'><img src={require('./Assets/icon_facebook.png')} alt='logo facebook'/></a>         
          </Col>
        </Row>
      </div>   
    );
  }
}

//home
class Home extends React.Component {
  render () {
    return (
      <div className="Body">
        <div className="column1">
          <img src={require('./Assets/banner.png')} alt='banner' style={{width: 271, height: 600}} />
        </div>

        <div className="column2">
          <Row className="rowApp">
            <Col>
              <ButtonImageText imgName='icon_napdt' name='Nạp tiền ĐT' link='/napdt'/>              
            </Col>
            <Col>
              <ButtonImageText imgName='icon_chuyentien' name='Chuyển tiền' link='/chuyentien'/>
            </Col>
            <Col>
              <ButtonImageText imgName='icon_nhantien' name='Nhận tiền' link='/nhantien'/>
            </Col>
            <Col>
              <ButtonImageText imgName='icon_thedt' name='Thẻ ĐT' link='/thedt' />
            </Col>        
          </Row>

          <Row className="rowApp">
            <Col>
                <ButtonImageText imgName='icon_dichvu' name='Dịch vụ' link='/dichvu' />
              </Col>
              <Col>
                <ButtonImageText imgName='icon_muasam' name='Mua sắm' link='/muasam' />
              </Col>
              <Col>
                <ButtonImageText imgName='icon_vephim' name='Vé phim' link='/vephim'/>
              </Col>
              <Col>
                <ButtonImageText imgName='icon_dien' name='Điện' link='/dien' />
              </Col>    
          </Row>

          <Row className="rowApp">
            <Col>
              <ButtonImageText imgName='icon_nuoc' name='Nước' link='/nuoc'/>
            </Col>
            <Col>
              <ButtonImageText imgName='icon_thedichvu' name='Thẻ dịch vụ' link='/thedichvu' />
            </Col>
            <Col>
              <ButtonImageText imgName='icon_vemaybay' name='Vé máy bay' link='/vemaybay' />
            </Col>
            <Col>
              <ButtonImageText imgName='icon_internet' name='Internet' link='/internet' />
            </Col>
          </Row>
          <Row className="rowApp">
            <Col>
              <ButtonImageText imgName='icon_truyenhinh' name='Truyền hình' link='/truyenhinh' />
            </Col>
            <Col>
              <ButtonImageText imgName='icon_tranothe' name='Trả nợ thẻ' link='/tranothe'/>
            </Col>
            <Col>
              <ButtonImageText imgName='icon_guiquamung' name='Gửi quà mừng' link='/guiquamung' />
            </Col>
            <Col>
              <ButtonImageText imgName='icon_chiatiennhom' name='Chia tiền nhóm' link='/chiatiennhom' />
            </Col>  
          </Row>
        </div>
    </div>
    );
  }
}

// app napdt
class NapDT extends React.Component {
  render () {
    return (
      <div className="Body">
        <div className="column1NapDT">
          <Input className="inputDT" type="text" placeholder="Nhập số  điện thoại ..." />
        </div>
        <div className="column2NapDT">
          <Row className="rowNapDT">
            <Col>
              <Button className="buttonNapDT">10k <br/> 9.530 <sup>đ</sup></Button>
            </Col>
            <Col>
              <Button className="buttonNapDT">20k <br/> 19.060 <sup>đ</sup></Button>
            </Col>
            <Col>
              <Button className="buttonNapDT">50k <br/> 47.650 <sup>đ</sup></Button>
            </Col>
          </Row>
          <Row className="rowNapDT">
            <Col>
              <Button className="buttonNapDT">100k <br/> 95.300 <sup>đ</sup></Button>
            </Col>
            <Col>
              <Button className="buttonNapDT">200k <br/> 190.600 <sup>đ</sup></Button>
            </Col>
            <Col>
            <Button className="buttonNapDT">300k <br/> 285.900 <sup>đ</sup></Button>
            </Col>
          </Row>
          <Row className="rowNapDT">
            <Col>
              <Button className="buttonNapDT">500k <br/> 476.500 <sup>đ</sup></Button>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

// app chuyentien
class ChuyenTien extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render () {
    return (
      <div className="Body">
        <div className="column1NapDT">
          <Nav vertical tabs className="Navbar">
            <NavItem>
              <NavLink onClick={() => { this.toggle('1');}}>Chuyển tiền đến TK ngân hàng</NavLink>
            </NavItem>
            <NavItem>
              <NavLink onClick={() => { this.toggle('2');}}>Chuyển tiền đến thẻ ngân hàng</NavLink>
            </NavItem>
            <NavItem>
              <NavLink onClick={() => { this.toggle('3');}}>Chuyển tiền đến bạn bè</NavLink>
            </NavItem>
          </Nav>
        </div>

        <div className="column2NapDT">
          <TabContent activeTab={this.state.activeTab} className="Navbar">
            <TabPane tabId="1">
              <Form>
                <FormGroup>
                  <Label>Chọn ngân hàng</Label>
                  <Input type="select">
                    <option>ViettinBank</option>
                    <option>Agribank</option>
                    <option>BIDV</option>
                    <option>VietcomBank</option>
                  </Input>
                </FormGroup>
                <FormGroup>
                  <Label>Số tài khoản người nhận</Label>
                  <Input type="number" placeholder="Nhập số tài khoản ..." />
                </FormGroup>
                <FormGroup>
                  <Label>Người nhận</Label>
                  <Input type="text" placeholder="Nhập tên người nhận ..." />
                </FormGroup>
                <FormGroup>
                  <Label>Số tiền (VND)</Label>
                  <Input type="number" placeholder="Nhập số tiền ..." />
                </FormGroup>
                <FormGroup>
                  <Label>Ghi chú</Label>
                  <Input type="text" placeholder="Nhập ghi chú (không bắt buộc) ..." />
                </FormGroup>
                <Button className="btnForm">Ok</Button>
              </Form>
            </TabPane>
            <TabPane tabId="2">
              <Form>
                <FormGroup>
                  <Label>Số thẻ người nhận</Label>
                  <Input type="number" placeholder="Nhập số thẻ ..." />
                </FormGroup>
                <FormGroup>
                  <Label>Người nhận</Label>
                  <Input type="text" placeholder="Nhập tên người nhận ..." />
                </FormGroup>
                <FormGroup>
                  <Label>Số tiền (VND)</Label>
                  <Input type="number" placeholder="Nhập số tiền ..." />
                </FormGroup>
                <FormGroup>
                  <Label>Ghi chú</Label>
                  <Input type="text" placeholder="Nhập ghi chú (không bắt buộc) ..." />
                </FormGroup>
                <Button className="btnForm">Ok</Button>
              </Form>
            </TabPane>
            <TabPane tabId="3">
              <Form>
                  <FormGroup>
                    <Label>Số điện thoại người nhận</Label>
                    <Input type="number" placeholder="Nhập số điện thoại người nhận ..." />
                  </FormGroup>
                  <Button className="btnForm">Ok</Button>
              </Form>
            </TabPane>
          </TabContent>
        </div>
       
      </div>
    );
  }
}

// app nhantien
class NhanTien extends React.Component {
  render () {
    return (
      <div className="Body">
        NhanTien
      </div>
    );
  }
}

// app thedt
class TheDT extends React.Component {
  render () {
    return (
      <div className="Body">
        <div className="column1NapDT">
          <div className="formTheDT">
            <Form>
              <FormGroup>
                <Label>Chọn nhà mạng</Label>
                <Input type="select">
                  <option>Viettel</option>
                  <option>Vinaphone</option>
                  <option>Mobiphone</option>
                  <option>VietnamMobile</option>
                </Input>
              </FormGroup>
              <FormGroup>
                <Label>Nhập số lượng</Label>
                <Input type="number" min="0" max="100"/>
              </FormGroup>
              <Button className="btnForm">Ok</Button>
            </Form>      
          </div>
        </div>
        <div className="column2NapDT">
          <Row className="rowNapDT">
              <Col>
                <Button className="buttonNapDT">10k <br/> 9.530 <sup>đ</sup></Button>
              </Col>
              <Col>
                <Button className="buttonNapDT">20k <br/> 19.060 <sup>đ</sup></Button>
              </Col>
              <Col>
                <Button className="buttonNapDT">50k <br/> 47.650 <sup>đ</sup></Button>
              </Col>
            </Row>
            <Row className="rowNapDT">
              <Col>
                <Button className="buttonNapDT">100k <br/> 95.300 <sup>đ</sup></Button>
              </Col>
              <Col>
                <Button className="buttonNapDT">200k <br/> 190.600 <sup>đ</sup></Button>
              </Col>
              <Col>
              <Button className="buttonNapDT">300k <br/> 285.900 <sup>đ</sup></Button>
              </Col>
            </Row>
            <Row className="rowNapDT">
              <Col>
                <Button className="buttonNapDT">500k <br/> 476.500 <sup>đ</sup></Button>
              </Col>
            </Row>
        </div>
      </div>
    );
  }
}

// app dichvu
class DichVu extends React.Component {
  render () {
    return (
      <div className="Body">
        Dich vu
      </div>
    );
  }
}

// app muasam
class MuaSam extends React.Component {
  render () {
    return (
      <div className="Body">
        Mua sam
      </div>
    );
  }
}

// app vephim
class VePhim extends React.Component {
  render () {
    return (
      <div className="Body">
        Ve phim
      </div>
    );
  }
}

// app dien
class Dien extends React.Component {
  render () {
    return (
      <div className="Body">
        <div className="column1NapDT">
          <div className="formTheDT">
            <Form>
              <FormGroup>
                <Label>Chọn nhà cung cấp</Label>
                <Input type="select">
                  <option>EVN Miền Trung</option>
                  <option>EVN Hà Nội</option>
                  <option>EVN Miền Bắc</option>
                  <option>EVN Hồ Chí Minh</option>
                </Input>
              </FormGroup>
              <FormGroup>
                <Label>Nhập mã khách hàng</Label>
                <Input type="number" />
              </FormGroup>
              <Button className="btnForm">Tiếp tục</Button>
            </Form>      
          </div>
        </div>
        <div className="column2NapDT">

        </div>
      </div>
    );
  }
}

// app nuoc
class Nuoc extends React.Component {
  render () {
    return (
      <div className="Body">
        <div className="column1NapDT">
          <div className="formTheDT">
            <Form>
              <FormGroup>
                <Label>Chọn nhà cung cấp</Label>
                <Input type="select">
                  <option>Cấp nước Bến Thành</option>
                  <option>Cấp nước Chợ Lớn</option>
                  <option>Cấp nước Cao Bằng</option>
                  <option>Cấp nước Đồng Nai</option>
                  <option>Cấp nước Huế</option>
                  <option>Cấp nước Hải Phòng</option>
                </Input>
              </FormGroup>
              <FormGroup>
                <Label>Nhập mã khách hàng</Label>
                <Input type="number" />
              </FormGroup>
              <Button className="btnForm">Tiếp tục</Button>
            </Form>      
          </div>
        </div>
        <div className="column2NapDT">

        </div>
      </div>
    );
  }
}

// app thedichvu
class TheDichVu extends React.Component {
  render () {
    return (
      <div className="Body">
        <div className="column1NapDT">
          <div className="formTheDT">
            <Form>
              <FormGroup>
                <Label>Chọn loại thẻ</Label>
                <Input type="select">
                  <option>Thẻ Zing</option>
                </Input>
              </FormGroup>
              <FormGroup>
                <Label>Nhập số lượng</Label>
                <Input type="number" min="0" max="100"/>
              </FormGroup>
              <Button className="btnForm">Thanh toán</Button>
            </Form>      
          </div>
        </div>
        <div className="column2NapDT">
          <Row className="rowNapDT">
              <Col>
                <Button className="buttonNapDT">10k <br/> 9.400 <sup>đ</sup></Button>
              </Col>
              <Col>
                <Button className="buttonNapDT">20k <br/> 18.800 <sup>đ</sup></Button>
              </Col>
              <Col>
                <Button className="buttonNapDT">50k <br/> 47.000 <sup>đ</sup></Button>
              </Col>
            </Row>
            <Row className="rowNapDT">
              <Col>
                <Button className="buttonNapDT">100k <br/> 94.000 <sup>đ</sup></Button>
              </Col>
              <Col>
                <Button className="buttonNapDT">200k <br/> 188.000 <sup>đ</sup></Button>
              </Col>
              <Col>
              <Button className="buttonNapDT">500k <br/> 470.000 <sup>đ</sup></Button>
              </Col>
            </Row>
            <Row className="rowNapDT">
              <Col>
                <Button className="buttonNapDT">1.000k <br/> 940.000 <sup>đ</sup></Button>
              </Col>
            </Row>
        </div>
      </div>
    );
  }
}

// app vemaybay
class VeMayBay extends React.Component {
  render () {
    return (
      <div className="Body">
        <div className="column1NapDT">
        </div>
        <div className="column2NapDT">
          <div className="formTheDT">
            <Form>
              <FormGroup>
                <Label>Điểm đi</Label>
                <Input type="select">
                  <option>Hồ Chí Minh - Tân Sơn Nhất (SGN)</option>
                  <option>Hà Nội - Nội Bài (HAN)</option>
                  <option>Đà Nẵng - Đà Nẵng (DAD)</option>
                  <option>Cần Thơ - Cần Thơ (VCA)</option>
                  <option>Đà Lạt - Liên Khương (DLI)</option>
                </Input>
              </FormGroup>
              <FormGroup>
                <Label>Điểm đến</Label>
                <Input type="select">
                  <option>Hồ Chí Minh - Tân Sơn Nhất (SGN)</option>
                  <option>Hà Nội - Nội Bài (HAN)</option>
                  <option>Đà Nẵng - Đà Nẵng (DAD)</option>
                  <option>Cần Thơ - Cần Thơ (VCA)</option>
                  <option>Đà Lạt - Liên Khương (DLI)</option>
                </Input>
              </FormGroup>
              <Row form>
                <Col xs={3}>
                  <FormGroup>
                    <Input type="radio"/>
                    <Label>Khứ hồi</Label>
                  </FormGroup>
                </Col>
                <Col xs={3}>
                  <FormGroup>
                    <Input type="radio" />
                    <Label>Một chiều</Label>
                  </FormGroup>
                </Col>
              </Row>
              <FormGroup>
                <Label>Ngày đi</Label>
                <Input type="date" />
              </FormGroup>
              <FormGroup>
                <Label>Số lượng</Label>
                <Input type="number" min="0"/>
              </FormGroup>
              <Row form>
                <Col xs={3}>
                  <FormGroup>
                    <Input type="checkbox"/>
                    <Label>Vietjet Air</Label>
                  </FormGroup>
                </Col>
                <Col xs={3}>
                  <FormGroup>
                    <Input type="checkbox"/>
                    <Label>VietnameAirlines</Label>
                  </FormGroup>
                </Col>
                <Col xs={3}>
                  <FormGroup>
                    <Input type="checkbox"/>
                    <Label>Jetstar</Label>
                  </FormGroup>
                </Col>
              </Row>
              <Button>Tìm Chuyến Bay</Button>
            </Form>
          </div>
        </div>
      </div>
    );
  }
}

// app internet
class Internet extends React.Component {
  render () {
    return (
      <div className="Body">
        <div className="column1NapDT">
          <div className="formTheDT">
            <Form>
              <FormGroup>
                <Label>Chọn nhà cung cấp</Label>
                <Input type="select">
                  <option>VNPT Cần Thơ</option>
                  <option>VNPT Đà Nẵng</option>
                  <option>VNPT Hà Nội</option>
                  <option>FPT Telecom</option>
                  <option>VNPT Hồ Chí Minh</option>
                  <option>HTV - TMS</option>
                </Input>
              </FormGroup>
              <FormGroup>
                <Label>Nhập mã khách hàng</Label>
                <Input type="number" />
              </FormGroup>
              <Button className="btnForm">Tiếp tục</Button>
            </Form>      
          </div>
        </div>
        <div className="column2NapDT">

        </div>
      </div>
    );
  }
}

// app truyenhinh
class TruyenHinh extends React.Component {
  render () {
    return (
      <div className="Body">
        <div className="column1NapDT">
          <div className="formTheDT">
            <Form>
              <FormGroup>
                <Label>Chọn nhà cung cấp</Label>
                <Input type="select">
                  <option>SCTV Hồ Chí Minh</option>
                  <option>VNPT Cần Thơ</option>
                  <option>VNPT Đà Nẵng</option>
                  <option>VTVCab</option>
                  <option>MobiTV</option>
                  <option>HTV-TMS</option>
                </Input>
              </FormGroup>
              <FormGroup>
                <Label>Nhập mã khách hàng</Label>
                <Input type="number" />
              </FormGroup>
              <Button className="btnForm">Tiếp tục</Button>
            </Form>      
          </div>
        </div>
        <div className="column2NapDT">

        </div>
      </div>
    );
  }
}

// app tranothe 
class TraNoThe extends React.Component {
  render () {
    return (
      <div className="Body">
        <div className="column1NapDT">
          <div className="formTheDT">
            <Form>
              <FormGroup>
                <Label>Số thẻ tín dụng</Label>
                <Input type="number" placeholder="Vui lòng nhập đúng số thẻ ..."/>
              </FormGroup>
              <FormGroup>
                <Label>Số tiền</Label>
                <Input type="number" placeholder="Nhâp số tiền ..."/>
              </FormGroup>
              <FormGroup>
                <Label>Tên</Label>
                <Input type="text" placeholder="Nhập tên gợi nhớ ..."/>
              </FormGroup>
              <Button className="btnForm">Tiếp tục</Button>
            </Form>
          </div>
        </div>
        <div className="column2NapDT">
        </div>
      </div>
    );
  }
}

// app gui qua mung
class GuiQuaMung extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '3'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  render () {
    return (
      <div className="Body">
        <div className="column1NapDT">
          <Nav vertical tabs className="Navbar">
            <NavItem>
              <NavLink onClick={() => { this.toggle('1');}}>Quà đã gửi</NavLink>
            </NavItem>
            <NavItem>
              <NavLink onClick={() => { this.toggle('2');}}>Quà đã nhận</NavLink>
            </NavItem>
            <NavItem>
              <NavLink onClick={() => { this.toggle('3');}}>Gửi quà mừng</NavLink>
            </NavItem>
          </Nav>
        </div>
        <div className="column2NapDT">
          <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId="1" className="Navbar">
              <ul>
                <li>Vy - 034 123 5641 - 10.000 <sup>đ</sup></li>
                <li>Vy - 034 123 5641 - 10.000 <sup>đ</sup></li>
                <li>Vy - 034 123 5641 - 10.000 <sup>đ</sup></li>
                <li>Vy - 034 123 5641 - 10.000 <sup>đ</sup></li>
                <li>Vy - 034 123 5641 - 10.000 <sup>đ</sup></li>
              </ul>
            </TabPane>
            <TabPane tabId="2" className="Navbar">
              <ul>
                <li>Yến - 054 231 6971 - 50.000 <sup>đ</sup></li>
                <li>Yến - 054 231 6971 - 50.000 <sup>đ</sup></li>
                <li>Yến - 054 231 6971 - 50.000 <sup>đ</sup></li>
                <li>Yến - 054 231 6971 - 50.000 <sup>đ</sup></li>
                <li>Yến - 054 231 6971 - 50.000 <sup>đ</sup></li>
              </ul>
            </TabPane>
            <TabPane tabId="3" className="Navbar">
              <Form>
                  <FormGroup>
                    <Label>Người nhận</Label>
                    <Input type="number" placeholder="Nhập số điện thoại người nhận ..." />
                  </FormGroup>
                  <FormGroup>
                    <Label>Số tiền</Label>
                    <Input type="number" placeholder="Nhập số tiền bạn muốn gửi (VND) ..." />
                  </FormGroup>
                  <Row form>
                    <Col xs={3}>
                      <FormGroup>
                        <Button className="btnForm">Chọn quà</Button>
                      </FormGroup>
                    </Col>
                    <Col xs={3}>
                      <FormGroup>
                        <Button className="btnForm">Ok</Button>
                      </FormGroup>
                    </Col>
                  </Row>
              </Form>
            </TabPane>
          </TabContent>
        </div>
      </div>
    );
  }
}

// app chia tien nhom
class ChiaTienNhom extends React.Component {
  render () {
    return (
      <div className="Body">
        Chia Tien Nhom
      </div>
    );
  }
}

// app so du
class SoDu extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }
  render () {
    return (
      <div className="Body">
        <div className="column1NapDT">
          <Nav vertical tabs className="Navbar">
            <NavItem>
              <NavLink onClick={() => { this.toggle('1');}}>Nạp tiền</NavLink>
            </NavItem>
            <NavItem>
              <NavLink onClick={() => { this.toggle('2');}}>Rút tiền</NavLink>
            </NavItem>
            <NavItem>
              <NavLink onClick={() => { this.toggle('3');}}>Chuyển tiền</NavLink>
            </NavItem>
          </Nav>
        </div>

        <div className="column2NapDT">
          <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId="1">
            <Row className="rowNapDT">
              <Col>
                <Button className="buttonNapTien">100.000 <sup>đ</sup></Button>
              </Col>
              <Col>
                <Button className="buttonNapTien">200.000 <sup>đ</sup></Button>
              </Col>
              <Col>
                <Button className="buttonNapTien">300.000 <sup>đ</sup></Button>
              </Col>
            </Row>
            <Row className="rowNapDT">
              <Col>
                <Button className="buttonNapTien">500.000 <sup>đ</sup></Button>
              </Col>
              <Col>
                <Button className="buttonNapTien">1.000.000 <sup>đ</sup></Button>
              </Col>
              <Col>
              <Button className="buttonNapTien">2.000.000 <sup>đ</sup></Button>
              </Col>
            </Row>
            <Row className="rowNapDT">
              <Col>
                <Button className="buttonNapTien">5.000.000 <sup>đ</sup></Button>
              </Col>
              <Col>
                <Button className="buttonNapTien">Số khác </Button>
              </Col>
            </Row>
            </TabPane>
            <TabPane tabId="2" className="Navbar">
              <Form>
                <FormGroup>
                  <Label>Chọn ngân hàng</Label>
                  <Input type="select">
                    <option>ViettinBank</option>
                    <option>Agribank</option>
                    <option>BIDV</option>
                    <option>VietcomBank</option>
                  </Input>
                </FormGroup>
                <FormGroup>
                  <Label>Số tài khoản</Label>
                  <Input type="number" placeholder="Nhập số tài khoản ..." />
                </FormGroup>
                <FormGroup>
                  <Label>Chủ tài khoản</Label>
                  <Input type="text" placeholder="Nhập tên chủ tài khoản ..." />
                </FormGroup>
                <FormGroup>
                  <Label>Số tiền (VND)</Label>
                  <Input type="number" placeholder="Nhập số tiền ..." />
                </FormGroup>
                <Button className="btnForm">Tiếp tục</Button>
              </Form>
            </TabPane>
            <TabPane tabId="3" className="Navbar">
              <Form>
                  <FormGroup>
                    <Label>Số điện thoại người nhận</Label>
                    <Input type="number" placeholder="Nhập số điện thoại người nhận ..." />
                  </FormGroup>
                  <Button className="btnForm">Ok</Button>
              </Form>
            </TabPane>
          </TabContent>
        </div>
      </div>
    );
  }
}

// app ngan hang
class NganHang extends React.Component {
  render () {
    return (
      <div className="Body">
        Ngân hàng
      </div>
    );
  }
}

// app thanh toan
class ThanhToan extends React.Component {
  render () {
    return (
      <div className="Body">
        Thanh toán
      </div>
    );
  }
}

// app Cai dat
class CaiDat extends React.Component {
  render () {
    return (
      <div className="Body">
        Cài đặt
      </div>
    );
  }
}
// app tkzalopay
class TKZaloPay extends React.Component {
  render () {
    return (
      <div className="Body">
        TK zalopay
      </div>
    );
  }
}


class App extends Component {
  render() {
    return (
      <Router>       

        <div className="App">
          <Header />
          
          <Route exact path="/" component={Home} />
          <Route path="/napdt" component={NapDT} />
          <Route path="/chuyentien" component={ChuyenTien} />
          <Route path="/nhantien" component={NhanTien} />
          <Route path="/thedt" component={TheDT} />
          <Route path="/dichvu" component={DichVu} />
          <Route path="/muasam" component={MuaSam} />
          <Route path="/vephim" component={VePhim} />
          <Route path="/dien" component={Dien} />
          <Route path="/nuoc" component={Nuoc} />
          <Route path="/thedichvu" component={TheDichVu} />
          <Route path="/vemaybay" component={VeMayBay} />
          <Route path="/internet" component={Internet} />
          <Route path="/truyenhinh" component={TruyenHinh} />
          <Route path="/tranothe" component={TraNoThe} />
          <Route path="/guiquamung" component={GuiQuaMung} />
          <Route path="/chiatiennhom" component={ChiaTienNhom} />
          <Route path="/sodu" component={SoDu} />
          <Route path="/thanhtoan" component={ThanhToan} />
          <Route path="/nganhang" component={NganHang} />
          <Route path="/caidat" component={CaiDat} />
          <Route path="/tkzalopay" component={TKZaloPay} />

          <Footer />          
        </div>
      </Router>
    );
  }
}

export default App;